variable "prefix" {
  default = "raad"
}
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "franciscogranados.1994@gmail"
}


variable "instance_type" {
  default = "t2.micro"
}
